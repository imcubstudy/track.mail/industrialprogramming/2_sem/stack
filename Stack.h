#ifndef TRACK_CPP_2_SEM_STACK_H
#define TRACK_CPP_2_SEM_STACK_H

#include "ChainedException.h"
#include "Array.h"

template <typename Data_T>
class Stack
{
private:
    DynamicArray<Data_T>    _data           = DynamicArray<Data_T>();
    static const int        _stretch_factor = 2;

public:
    Stack() = default;

    explicit Stack( size_t capacity )
    { _data.resize( capacity ); }

    Stack( const Stack& that )  = default;

    Stack( Stack&& that )
    { _data = std::move( that._data ); }

    ~Stack()    = default;

    Stack&      operator=( const Stack& that ) = default;

    Stack&      operator=( Stack&& that )
    { _data = std::move(that._data); return *this; }

    Data_T      pop()
    {
        InvokeCthulhu( _data.valid(),
           {
               if( _data.size() == 0 ) throw new (std::nothrow) ChainedException( "Pop from empty stack" );

               return _data.pop();

           }, "Data array is invalid" );
    }

    Data_T&     top()
    {
        InvokeCthulhu( _data.valid(),
           {
               if( _data.size() == 0 ) throw new (std::nothrow) ChainedException( "Top from empty stack" );

               return _data[_data.size() - 1];

           }, "Data array is invalid" );
    }

    size_t      push( const Data_T& value )
    {
        InvokeCthulhu( _data.valid(),
           {
               if(      _data.capacity() == 0 )             _data.resize( 1 );
               else if( _data.capacity() == _data.size() )  _data.resize( _data.size() * _stretch_factor );

               return _data.push( value );

           }, "Data array is invalid" );
    }

    bool        valid()
    {
        InvokeCthulhu( true, { return _data.valid(); }, "Data array is invalid" );
    }

    size_t      size()
    {
        InvokeCthulhu( _data.valid(), { return _data.size(); }, "Data array is invalid" );
    }

    size_t      capacity()
    {
        InvokeCthulhu( _data.valid(), { return _data.capacity(); }, "Data array is invalid" );
    }
};

#endif //TRACK_CPP_2_SEM_STACK_H
