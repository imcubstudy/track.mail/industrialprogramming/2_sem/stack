#include <iostream>
#include <fstream>
#include "Stack.h"

int main()
{
    try
    {
        Stack<int> stack(10);
        for( int i = 0; i < 20; i++ )
        {
            stack.push( i );
        }

        for( int i = 30; i >= 0; i-- )
            std::cout << stack.pop() << std::endl;
    }
    catch( ChainedException* e )
    {
        std::ofstream   out( "Result.txt" );
        e->dump( out );
        std::cout << e->what();
    }
    return 0;
}

